﻿namespace OtusHW8.Interfaces
{
    public interface IThirdLevelClass
    {
        public string ThirdLevelName { get; set; }
    }
}
