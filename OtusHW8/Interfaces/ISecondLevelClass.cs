﻿namespace OtusHW8.Interfaces
{
    public interface ISecondLevelClass
    {
        public string SecondLevelName { get; set; }
    }
}
