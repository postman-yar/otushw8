﻿namespace OtusHW8.Interfaces
{
    public interface IFirstLevelClass
    {
        public string FirstLevelName { get; set; }
    }
}
