﻿namespace OtusHW8.Interfaces
{
    public interface IMyClonable<T>
    {
        public T MyClone();
    }
}
