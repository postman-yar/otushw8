﻿using System;
using System.Collections.Generic;
using System.Linq;
using OtusHW8.Interfaces;

namespace OtusHW8
{
    public class SecondLevelClass : FirstLevelClass, ISecondLevelClass, ICloneable, IMyClonable<SecondLevelClass>
    {
        public string SecondLevelName { get; set; }

        public SecondLevelClass(SecondLevelClass obj) : base(obj)
        {
            SecondLevelName = obj.SecondLevelName;
        }

        public SecondLevelClass()
        {
            SecondLevelName = "Second level class";
        }

        public new object Clone()
        {
            return new SecondLevelClass(this);
        }

        public override string ToString()
        {
            return base.ToString() + " / " + SecondLevelName;
        }

        public new SecondLevelClass MyClone()
        {
            return new SecondLevelClass(this);
        }
    }
    
}
