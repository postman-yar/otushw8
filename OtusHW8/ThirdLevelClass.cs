﻿using OtusHW8.Interfaces;

namespace OtusHW8
{
    public class ThirdLevelClass : SecondLevelClass, IThirdLevelClass, ICloneable, IMyClonable<ThirdLevelClass>
    {
        public string ThirdLevelName { get; set; }

        public ThirdLevelClass()
        {
            ThirdLevelName = "Third level class";
        }

        public ThirdLevelClass(ThirdLevelClass obj) : base(obj)
        {
            ThirdLevelName = obj.ThirdLevelName;
        }

        public new object Clone()
        {
            return new ThirdLevelClass(this);
        }

        public override string ToString()
        {
            return base.ToString() + " / " + ThirdLevelName;
        }

        public new ThirdLevelClass MyClone()
        {
            return new ThirdLevelClass(this);
        }
    }
}
