﻿using System;
using System.Collections.Generic;
using System.Linq;
using OtusHW8.Interfaces;

namespace OtusHW8
{
    public class FirstLevelClass : IMyClass, IFirstLevelClass, ICloneable, IMyClonable<FirstLevelClass>
    {
        public string FirstLevelName { get; set; }

        public FirstLevelClass()
        {
            FirstLevelName = "First level class";
        }

        public FirstLevelClass(FirstLevelClass obj)
        {
            FirstLevelName = obj.FirstLevelName;
        }
        public object Clone()
        {
            return new FirstLevelClass(this);
        }

        public override string ToString()
        {
            return base.ToString() + " / " + FirstLevelName;
        }

        public FirstLevelClass MyClone()
        {
            return new FirstLevelClass(this);
        }
    }
}
