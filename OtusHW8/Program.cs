﻿using OtusHW8;

var fooThirdLevelClass = new ThirdLevelClass();
var iClonableFooThirdLevelClass = (ThirdLevelClass)fooThirdLevelClass.Clone();

Console.WriteLine("IClonable");
Console.WriteLine($"Неповторимый оригинал - {fooThirdLevelClass}");
Console.WriteLine($"Жалкая пародия - {iClonableFooThirdLevelClass}");

var myCloneFooThirdLevelClass = fooThirdLevelClass.MyClone();

Console.WriteLine("MyClonable");
Console.WriteLine($"Неповторимый оригинал - {fooThirdLevelClass}");
Console.WriteLine($"Жалкая пародия - {myCloneFooThirdLevelClass}");