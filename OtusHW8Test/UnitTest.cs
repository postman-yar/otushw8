using OtusHW8;

namespace OtusHW8Test
{
    public class UnitTest
    {
        [Fact]
        public void ClonableTest()
        {
            var fooThirdLevelClass = new ThirdLevelClass();
            var iClonableFooThirdLevelClass = (ThirdLevelClass)fooThirdLevelClass.Clone();
            Assert.Equal(fooThirdLevelClass.ToString(), iClonableFooThirdLevelClass.ToString());
        }

        [Fact]
        public void MyClonableTest()
        {
            var fooThirdLevelClass = new ThirdLevelClass();
            var myCloneFooThirdLevelClass = fooThirdLevelClass.MyClone();
            Assert.Equal(fooThirdLevelClass.ToString(), myCloneFooThirdLevelClass.ToString());
        }
    }
}